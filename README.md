# 番茄信息发布小程序

#### 介绍
主要功能有共享朋友圈，附近人信息，名片生成，商家入住等功能...
源码只用于学习，如有商用请联系开发者。

#### 软件架构
Codeigniter + layui + vant-weapp + mysql


#### 安装教程

1. 新建数据库，导入根目录下的hbsq.sql
2. 修改apps->config下的database.php和constants.php文件，分别是数据库链接和小程序appid资料。按里面的提示说明修改即可。
3. 小程序代码在根目录的xcx目录里面，新建小程序导入即可。
4. 小程序需要修改一个文件app.js 里面的 host 变量修改为你的域名。
5. 最后一步把伪静态设置了，就完事了。
如果你使用的是apache那就不用管了，根目录下已经放了静态规则。 nginx配置如下 
`if (!-f $request_filename) { rewrite ^/.*$ /index.php last; }`
6. 后台还没有完善，有兴趣的朋友可以一起完善一下...

#### 演示小程序二维码
[演示链接](https://www.chaituans.com/news/id-10.html)


