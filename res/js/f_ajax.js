/***
 * f_ajax 
 * by chaituan@126.com
 */
var f_ajax = function(lay_data) {
	var val = lay_data.elem.attributes;
	var str1,str2,str3;
	if(typeof(val.callBefore)!= "undefined"){
		str1 = {"callBefore":val.callBefore.nodeValue};
	}
	if(typeof(val.callBack)!= "undefined"){
		str2 = {"callBack":val.callBack.nodeValue};
	}
	str3 = {"url":val.url.nodeValue,"method":lay_data.form.method,"location":val.location.nodeValue,"tips":typeof(val.tips)!= "undefined"?val.tips.nodeValue:'',"datatype":typeof(val.datatype)!= "undefined"?val.datatype.nodeValue:'json'};
	var options = $.extend({},str1,str2,str3);
	var defaults = {
		callbackDelay: 1000, /* callback delay */
		threadLock: true, /* 是否开启线程锁定，防止短时间内多次点击 */
		timeInterval: 3000, /* 两次操作的时间间隔， 如果没有开启线程锁定，则此项无效*/
		method: 'post', /* 默认数据传输方式 */
		callBefore: function () {        /* 在执行ajax操作之前的调用函数 */
			//do nothing here
			return true;
		},
		callBack : function (data) {   /* 执行ajax之后的回调函数 */
			try {
				layer.msg(data.message,{icon:data.state});
			} catch(e) {
				layer.alert(data.message);
			}
		}
	}

	//merge parameters
	options = $.extend(defaults,options);
	
	sub(options);//开始
	
	function sub(options) {
		var params = getParams(options);
		/* 再次合并参数 */
		options = $.extend(options, params);
		/* 请求发送之前的调用函数 */
		if (!options.callBefore()) return false;
		if(options.tips=='ok'){
			layer.confirm('确定要执行当前操作？', {
				  btn: ['是','否'] //按钮
				}, function(index){
					if (options.method == 'get') {
						ajaxGet(options, this);  //发送get请求
					} else {
						ajaxPost(options, this); //发送post请求
					}
					layer.close(index);
				});
			return false;
		}else{
			if (options.method == 'get') {
				ajaxGet(options, this);  //发送get请求
			} else {
				ajaxPost(options, this); //发送post请求
			}

		}
		return false;
	}

	/**
	 * 根据元素的属性获取参数
	 * @param       obj     当前点击的元素
	 */
	function getParams(data) {
		var params = data;
		if ($.type(params.callBefore) == 'string') {
			params.callBefore = new Function("data", 'return '+params.callBefore);
		}
		if ($.type(params.callBack) == 'string') params.callBack = new Function("data", params.callBack);
		return params;
	}

	/**
	 * 以get方式发送请求并处理返回参数
	 * @param       params   请求参数
	 * @param       obj     当前被点击的对象
	 */
	function ajaxGet(params, obj) {

		if ( params.threadLock ) {       /* 锁定线程，防止多次点击 */
			btnLoading(obj);
		}
		var loads = layer.load(1, {shade: [0.5,'#000']});
		$.get(params.url, {run: Math.random()}, function (result) {
			switch ( params.datatype ) {
				case 'html':
					params.callBack(result);
					break;
				case 'json':
					var data = result;
					params.callBack(data);
					location(data);
					break;
				default :
					layer.alert('不支持的数据格式');
			}
		},params.datatype).fail(function(){
			layer.alert('网络异常，请联系客服');
		});
		if ( params.threadLock ) {       /* 解除锁定 */
			setTimeout(function () {
				layer.close(loads);
				btnReset(obj);
			}, options.timeInterval);
		}
	}

	/**
	 * 以post方式发送请求,通常要处理表单
	 * @param   params   请求参数
	 * @param   obj     当前被点击的对象
	 * @return boolean
	 */
	function ajaxPost(params, obj) {		
		
		//locking thread to prevent multiple clicks.
		if ( params.threadLock ) {
			btnLoading(obj);
		}
		//collect form data
		var formData = lay_data.field;
		var loads = layer.load(1, {shade: [0.5,'#000']});
		$.post(params.url, formData, function (result) {
			switch ( params.datatype ) {
				case 'html':
					params.callBack(result);
					break;
				case 'json':
					var data = result;
					params.callBack(data);
					location(data);
					break;
				default :
					layer.alert('不支持的数据格式');
			}
		},params.datatype).fail(function(){
			layer.alert('网络异常，请联系客服');
		});
		if ( params.threadLock ) {       /* 解除锁定 */
			setTimeout(function () {
				layer.close(loads);
				btnReset(obj);
			}, params.timeInterval);
		}
	}

	//使按钮进入加载状态
	function btnLoading(obj) {
		try {
			$(obj).css("pointer-events",'none');
		} catch (e) {
			$(obj).attr('disabled', true);
		}
	}

	//恢复按钮状态
	function btnReset(obj) {
		try {
			$(obj).removeAttr("style");
		} catch (e) {
			$(obj).attr('disabled', false);
		}
	}

	//处理回调后的跳转
	function location(data) {
		var location = options.location;
		if ( location && data.state == 1 ) {
			if ( location == 'reload' ) { //刷新页面
				setTimeout(function() {
					window.location.reload();
				}, options.callbackDelay);
			} else  {
				setTimeout(function() {
					window.location.replace(location);   //跳转
				}, options.callbackDelay);
			}
		}
		if(data.state == 1){
			setTimeout(function() {
				layer.closeAll();   //跳转
			}, 2000);
		}
	}
}
