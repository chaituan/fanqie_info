<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				
				<div class="layui-inline">
					<?php echo admin_btn(site_url($add_url),'add','layui-btn-normal');?>
				</div>
		</blockquote>
		<form class="layui-form" method="post">
			<table class="layui-table">
			    <thead>
					<tr>
						<th>分组key</th>
						<th>变量</th>
						<th>变量名</th>
						<th>变量值</th>
						<th>输入</th>
						<th>说明</th>
						<th>操作</th>
					</tr>
				</thead>
				<tbody>
                    <?php if (empty($items)){?>
                    <tr>
						<td class="empty-table-td"><?php echo $emptyRecord;?></td>
					</tr>
	                <?php }else{ foreach ($items as $key=>$v){ ?>
                    <tr>
						<td><?php echo $v['tkey'];?></td>
						<td><?php echo $v['cname'];?></td>
						<td><?php echo $v['key'];?></td>
						<td title="<?php echo $v['val'];?>"><?php echo str_cut($v['val'],40);?></td>
						<td><?php echo $v['imports'];?></td>
						<td><?php echo mb_substr($v['bak'],0,20);?></td>
						<td>
						<div class="layui-btn-group">
						  <?php echo admin_btn(site_url("$dr_url/edit/id-".$v['id']),'edit','layui-btn-xs');?>
						  <?php echo admin_btn(site_url("$dr_url/del/id-".$v['id']),'del','layui-btn-xs f_del');?>
						</div>
						</td>
					</tr>
                    <?php }}?>
				</tbody>
			</table>
	</div>
</div>

<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>