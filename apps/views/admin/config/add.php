<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">添加</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form a-e-form" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">分组</label> 
				<div class="layui-input-block">
					<select  name="data[tkey]">
	                    <?php foreach ($__menuGroups as $v){?>
							<option value="<?php echo $v['tkey'];?>"><?php echo $v['name'];?></option>
						<?php }?>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">变量</label>
				<div class="layui-input-block"> 
					<input type="text" class="layui-input" name="data[cname]" placeholder="中文" max-length="30"  lay-verify="required">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">变量名</label> 
				<div class="layui-input-block">
					<input type="text" class="layui-input" name="data[key]" placeholder="英文" max-length="30" lay-verify="required">
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">变量值</label>
				<div class="layui-input-block">
					<textarea class="layui-textarea" name="data[val]"  lay-verify="required"></textarea>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">输入</label> 
				<div class="layui-input-block">
					<select name="data[imports]">
						<option value="text">文本框</option>
						<option value="textarea">多行文本框</option>
						<option value="radio">单选框</option>
					</select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">变量说明</label>
				<div class="layui-input-block">
					<textarea class="layui-textarea" name="data[bak]" max-length="100" ></textarea>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<?php echo admin_btn($add_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
				</div>
			</div>
		</form>
	</div>
</div>			
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>			
			
			
			