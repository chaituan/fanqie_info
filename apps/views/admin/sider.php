<!-- 左侧导航 -->
<div class="layui-side layui-bg-black">
	<div class="user-photo">
		<a class="img" title="我的头像"><img src="<?php echo IMG_PATH.'admin/adminloginlogo.png'?>"></a>
		<p>
			Hello！<span class="userName"><?php echo $loginUser['username'];?></span>
		</p>
	</div>
	<div class="navBar layui-side-scroll"></div>
	<div class="layui-side-scroll">
		<!-- 左侧导航区域（可配合layui已有的垂直导航） -->
		<ul class="layui-nav layui-nav-tree" lay-filter="test">
		<?php $i = 0; foreach($__menuGroups as $key => $val ){if(empty($systemMenu [$val ['tkey']])){continue;}?>
			<li class="layui-nav-item <?php if($val['tkey'] == $mgroup || ($i == 0 && $mgroup=='')){ echo 'layui-nav-itemed';}?>">
				<a class="" href="javascript:;"><i class="fa fa-fw fa-<?php echo $val['icon']; ?> sider"></i><cite><?php echo $val['name']; ?></cite></a>
				<dl class="layui-nav-child">
					<?php  $k = $val['tkey']; foreach ($systemMenu["$k"] as $keys=>$group){    ?>
						<?php foreach($group['sub'] as $v){?>
						<dd  <?php if($v['url']==$currentOpt || $mid==$v['id']){ echo 'class="layui-this"';}?>>
						<a href="<?php echo site_url($v['url'].'/m-'.$v['id'])?>"><i class="fa fa-fw fa-circle-o" style="font-size: 10px;margin-right: 5px;"></i><?php echo $v['name']; ?></a>
						</dd>
					<?php }}?>
				</dl>
			</li>
		<?php }?>
		</ul>
	</div>
</div>
