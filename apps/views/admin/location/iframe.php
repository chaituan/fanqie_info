<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8" />
<title><?php echo SYSTEM_NAME;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="author" content="chaituan@126.com">
<link rel="stylesheet" href="<?php echo LAYUI."css/layui.css";?>" type="text/css" />
<link rel="stylesheet" href="<?php echo CSS_PATH."admin/main.css";?>" type="text/css" />
<link rel="stylesheet" href="<?php echo CSS_PATH."font-awesome.min.css";?>" type="text/css" />
</head>
<body >
<div class="layui-layout">
<div>
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<form class="layui-form" action="">
					<div class="layui-inline">
					    <div class="layui-input-inline">
					    	<input type="text" value="<?php echo isset($name)?$name:'';?>" name="name" placeholder="区域" class="layui-input" lay-verify='required'>
					    </div>
					    <?php echo admin_btn('', 'find',"")?>
					</div>
				</form>
		</blockquote>
		<form class="layui-form" method="post">
			<table class="layui-table">
					<thead>
						<tr>
							<th><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose" ></th>
							<th>ID</th>
							<th>区域</th>
						</tr>
					</thead>
					<tbody>
                        <?php if (empty($items)){ ?>
						<tr>
							<td class="empty-table-td"><?php echo $emptyRecord;?></td>
						</tr>
                        <?php }else{ foreach ($items as $k=>$v){ ?>
                        <tr>
	                        <td>
	                           <input type="checkbox" name="xz" ckd="ckd" value="<?php echo $v['id']?>" data-name="<?php echo $v['lname'];?>" lay-skin="primary" >
	                        </td>
							<td><?php echo $v['id'];?></td>
							<td><?php echo $v['lname'];?></td>
						</tr>
                        <?php }}?>
					</tbody>
			</table>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/javascript">
var tipsi;
$(".hover").hover(function(){
   tipsi = layer.tips($(this).data('t'),this,{tips:1,time:0});
 },function(){
	layer.close(tipsi);
 });
</script>
</div>
</body>
</html>