<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
			<form class="layui-form" action="">
				<div class="layui-inline">
				    <div class="layui-input-inline">
				    	<input type="text" value="<?php echo $val;?>" name="val" placeholder="请输入分组名称" class="layui-input" lay-verify='required'>
				    </div>
				    <?php echo admin_btn('', 'find',"")?>
				</div>
				<div class="layui-inline f-right">
					<?php echo admin_btn(site_url($add_url),'add','layui-btn-normal');?>
				</div>
			</form>
		</blockquote>
		<form class="layui-form" method="post">
		  	<table class="layui-table">
			    <thead>
					<tr>
						<th><input type="checkbox" name="" lay-skin="primary" lay-filter="allChoose"></th>
						<th>ID</th>
						<th>分组名称</th>
						<th>分组key</th>
						<th>图标icon</th>
						<th>排序数字</th>
						<th>添加时间</th>
						<th>操作</th>
					</tr> 
			    </thead>
			    <tbody>
                    <?php if (empty($items)){?>
                    <tr>
						<td class="empty-table-td"><?php echo $emptyRecord;?></td>
					</tr>
                    <?php }?>
					<?php foreach ($items as $k=>$v){ ?>
                     <tr id="list_<?php echo $v['id']?>">
                        <td>
                           <input type="checkbox" name="checked[<?php echo $k;?>]" ckd="ckd" lay-skin="primary" >
                        </td>
						<td>
                           <?php echo $v['id']; ?>
                           <input type="hidden" name="hids[<?php echo $v['id']; ?>]" value="<?php echo $v['id']; ?>" />
						</td>
						<td><input type="text" name="data[<?php echo $v['id']; ?>][name]" value="<?php echo $v['name']; ?>" class="layui-input" /></td>
						<td><input type="text" name="data[<?php echo $v['id']; ?>][tkey]" value="<?php echo $v['tkey']; ?>" class="layui-input" /></td>
						<td><input type="text" name="data[<?php echo $v['id']; ?>][icon]" value="<?php echo $v['icon']; ?>" class="layui-input" /></td>
						<td><input type="text" name="data[<?php echo $v['id']; ?>][sort_num]" value="<?php echo $v['sort_num']; ?>" class="layui-input" /></td>
						<td><?php echo date('Y-m-d',$v['add_time']); ?></td>
						<td>
						<div class="layui-btn-group">
						  <?php echo admin_btn(site_url($dr_url.'/edit/id-'.$v['id']),'edit','layui-btn-xs');?>
						  <?php echo admin_btn(site_url($dr_url.'/del/id-'.$v['id']),'del','layui-btn-xs f_del');?>
						</div>
						</td>
					</tr>
                    <?php }?>
	             </tbody>
			</table>
			<footer class="panel-footer">
				<div class="layui-row">
				    <div class="layui-col-xs6">
				    	<div class="layui-inline">
							<?php echo admin_btn(site_url($dr_url.'/quicksave'), 'save','',"lay-filter='sub' location=''");?>
						</div>
				      	<div class="layui-inline">
							<?php echo admin_btn(site_url($dr_url.'/dels'),'dels','layui-btn-danger',"lay-filter='dels' location='' callBack='dels_row(data)'");?>
						</div>
						<div class="layui-inline">
							<?php echo admin_btn(site_url($dr_url.'/export'), 'exp','layui-btn-normal');?>
						</div>
				    </div>
				    <div class="layui-col-xs6 ">
				      	<div class="layui-box layui-laypage layui-laypage-default f-right footer-page" >
				      	<?php echo $pagemenu;?>
				      	</div>
				    </div>
				</div>
			</footer>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>