<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">编辑</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form  a-e-form" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">分组名称</label>
				<div class="layui-input-block">
					<input type="text" name="data[name]" class="layui-input"  value="<?php echo $item['name']?>" required>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">分组key</label>
				<div class="layui-input-block">
					<input type="text" name="data[tkey]" class="layui-input" value="<?php echo $item['tkey'];?>" required> 
					<input type="hidden" name="tkey_bak" value="<?php echo $item['tkey']?>">
				</div>
			</div>

			<div class="layui-form-item">
				<label class="layui-form-label">分组图标</label>
				<div class="layui-input-block">
					<input type="text" name="data[icon]" class="layui-input" value="<?php echo $item['icon']?>" >
				</div>
			</div>

			<div class="layui-form-item">
				<label class="layui-form-label">排序数字</label>
				<div class="layui-input-block">
					<input type="text" name="data[sort_num]" class="layui-input" dtype="number" value="<?php echo $item['sort_num']?>" placeholder="排序数字" required>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="col-sm-offset-2 layui-input-block">
					<input type="hidden" name="id" value="<?php echo $item['id'];?>" /> 
					<?php echo admin_btn($edit_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
				</div>
			</div>
		</form>
	</div>
</div>
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>