<?php echo template('admin/header');template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<div class="row">
			<div class="sysNotice col">
				<blockquote class="layui-elem-quote title">订单详情</blockquote>
						<table class="layui-table">
						<colgroup><col width="150"><col><col width="150"><col></colgroup>
						<tbody>
							<tr>
								<td>订单编号</td><td ><?php echo $item['oid'];?></td>
								<td>订单状态</td>
								<td >
									<?php 
										if($item['state']==2){echo "<span class='btn btn-xs btn-success'>已支付</span> ";
										}elseif($item['state']==3){echo "<span class='btn btn-xs btn-warning'>申请退款</span> ";
										}elseif($item['state']==4){echo "<span class='btn btn-xs btn-danger'>退款成功</span> ";
										}elseif($item['state']==5){echo "<span class='btn btn-xs btn-primary'>订单结束</span>";}
									?>
								</td>
							</tr>
							<tr>
								<td>商品名字</td><td ><?php echo ($item['gname']);?></td>
								<td>商品数量</td><td >1</td>
							</tr>
							<tr>
								<td>下单时间</td><td ><?php echo format_time($item['addtime']);?></td>
								<td>支付时间</td><td ><?php echo format_time($item['paytime']);?></td>
							</tr>
							<tr>
								<td>商品价格</td><td><?php echo $item['price'];?></td>
			              		<td>实际支付</td><td ><?php echo $item['pay_price'];?></td>
							</tr>
							<tr>
								<td>微信单号</td><td colspan="3"><?php echo $item['transaction_id'];?></td>
							</tr>
							<tr>
								<td>买家手机</td><td colspan="3"><?php echo $item['mobile'];?></td>
							</tr>
						</tbody>
				</table>
			</div>
		</div>
		
		
		<?php if($item['state']==3){?>
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">确认退货（系统会自动退款到用户零钱中）</div>
		</blockquote>
		<form class="layui-form a-e-form" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">退款金额</label>
				<div class="layui-input-block">
					<input type="text" name="data[total]" class="layui-input" value="<?php echo $item['pay_price']?>" cname="金额" lay-verify="required" >
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="hidden" name="data[id]" value="<?php echo $item['id']?>">
					<input type="hidden" name="data[gid]" value="<?php echo $item['gid']?>">
					<input type="hidden" name="data[uid]" value="<?php echo $item['uid']?>">
					<input type="hidden" name="data[integral]" value="<?php echo $item['integral']?>">
					<input type="hidden" name="data[wx_oid]" value="<?php echo $item['transaction_id']?>">
					<?php echo admin_btn('/adminct/order/back','save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
				</div>
			</div>
		</form>
		<?php }?>
	</div>
</div>
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>