<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote news_search">
				<div class="layui-inline">
					<form class="layui-form">
						<div class="layui-input-inline">
							<select name="sid" id="sid" id="state">
								<option value="0">全部商家</option>
								<?php foreach ($shop as $k=>$v){?>
								<option value="<?php echo $v['id'];?>"><?php echo $v['sname'];?></option>
								<?php }?>
				        	</select>
					    </div>
						<div class="layui-input-inline">
							<select name="state" id="state">
								<option value="0">订单状态</option>
								<?php foreach ($state as $k=>$v){?>
								<option value="<?php echo $k;?>"><?php echo $v;?></option>
								<?php }?>
				        	</select>
					    </div>
						<div class="layui-input-inline">
							<input type="text"  id="srk" name="srk" placeholder="请输入订单号" class="layui-input" >
						</div>
				    	<?php echo admin_btn('', 'find',"",'lay-filter="order-find"')?>
					</form>
				</div>
		</blockquote>
		<table  id="common" lay-filter="common" ></table>
	</div>
</div>

<?php echo template('admin/script');?>
<script type="text/html" id="operation">
	<?php echo admin_btn(site_url($dr_url.'/detail/id-{{d.id}}'),'','layui-btn-xs layui-btn-normal','','详');?>
	<?php echo admin_btn(site_url($dr_url.'/del/id-{{d.id}}'),'del','layui-btn-xs f_del_d','lay-event="del"');?>
</script>
<script type="text/html" id="zt">
{{# if(d.state==2){ }}
<span class='layui-btn layui-btn-xs layui-bg-orange'>已支付</span>
{{# }else if(d.state==3){ }}
<span class='layui-btn layui-btn-xs layui-bg-green'>申请退款</span>
{{# }else if(d.state==4){ }}
<span class='layui-btn layui-btn-xs layui-bg-cyan'>退款成功</span>
{{# }else if(d.state==5){ }}
<span class='layui-btn layui-btn-xs layui-bg-blue'>订单结束</span>
{{# } }}
</script>
<script>
//执行渲染
layui.table.render({
	elem: '#common',
	id:'common',
	height: 'full-250',
	url:'<?php echo site_url("$dr_url/lists")?>',
	cols: [[
	       {checkbox: true},
	       {type:'numbers',title: 'No.'},
	       {field: 'id', title: 'ID', width: 80},
	       {field: 'oid', title: '订单号'},
	       {field: 'gname', title: '名称'},
	       {field: 'pay_price', title: '总价'},
	       {field: 'mobile', title: '电话'},
	       {field: 'integral', title: '使用积分'},
	       {field: 'xxx', title: '状态',toolbar:"#zt", width: 120},
	       {field: 'addtime', title: '下单时间',toolbar:'<div>{{Time(d.addtime, "%y-%M-%d %h:%m:%s")}}</div>'},
	       {field: 'right', title: '操作',toolbar: '#operation'}
	       ]],
	limit: 20,
	page:true,
	response:{msgName:'message'},
	done:function(res, curr, count){
		this.where.total = count;
		layer.photos({photos:'.img_view'});//添加预览
	}
});
//数据数据 异步提交查询
layui.form.on('submit(order-find)',function(){
	layui.table.reload('common',{//这里的find 是为了后台数据处理
		where:{state:$('#state').val(),srk:$('#srk').val(),sid:$('#sid').val(),find:'find',total:''},
		done:function(res, curr, count){
			this.where.total = count;
			this.where.find = '';
			layer.photos({photos:'.img_view'});//添加预览
		}
	});
	return false;
});
</script>