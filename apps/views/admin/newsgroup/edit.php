<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">添加 <span class="layui-badge layui-bg-orange">注：本系统只支持二级分类，添加多级不会显示，如需多级请联系客服开发</span></b></div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form a-e-form" method="post">
						<div class="layui-form-item">
							<label class="layui-form-label"> 上级</label>
							<div class="layui-input-block">
								<select name="data[parent_id]" >
									<option value="0">顶级</option>
                                	<?php foreach($parent as $v){?>
                                	<option value="<?php echo $v['id'];?>" <?php if($v['id']==$item['parent_id']) echo "selected";?>>  <?php echo str_repeat('├',$v['level']).' '.$v['catname'];?></option>
                                	<?php }?>
                            	</select>
							</div>
						</div>
						<div class="layui-form-item">
							<label class="layui-form-label"> 分类名称</label>
							<div class="layui-input-block">
								<input type="text" name="data[catname]" class="layui-input" value="<?php echo $item['catname']?>" lay-verify="required">
							</div>
						</div>
						<div class="layui-form-item">
							<div class="layui-input-block">
							<input type="hidden" name="id" value="<?php echo $item['id']?>">
							<?php echo admin_btn($edit_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo template('admin/script');?>
<?php echo template('admin/footer');?>