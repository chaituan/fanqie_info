<?php echo template('admin/header');echo template('admin/sider');?>
<div class="layui-body">
	<div class="childrenBody childrenBody_show">
		<blockquote class="layui-elem-quote a-e-quote">
				<div class="layui-inline">添加</div>
				<div class="layui-inline f-right"><?php echo admin_btn($index_url, '', 'layui-btn-xs','','返回')?></div>
		</blockquote>
		<form class="layui-form  a-e-form" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">定位区域</label>
				<div class="layui-input-block">
					<span class="layui-btn layui-btn-primary" id="add_location">添加</span>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">分组</label>
				<div class="layui-input-block">
					<select name="data[gid]" data-toggle="select" >
		            <?php foreach ($group as $v){?>
		            	<option value="<?php echo $v['id'];?>"> <?php echo $v['aname'];?></option>
		            <?php }?>
		        </select>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">标题</label>
				<div class="layui-input-block">
					<input type="text" name="data[title]" class="layui-input" lay-verify="required">
				</div>
			</div>			
			<div class="layui-form-item">
				<label class="layui-form-label">URL</label>
				<div class="layui-input-block">
					<input type="text" name="data[url]" class="layui-input"   >
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">图片</label>
				<div class="layui-input-block">
					<?php echo admin_btn(site_url('/images/upload'), 'file','layui-btn-primary f_file');?><span class="thumb-say">640px * 340px</span>
					<div class="layui-upload layui-hide">
					  	<div class="layui-upload-list"><table class="layui-table"><tbody class="file_list"></tbody></table></div>
					  	<?php echo admin_btn('', 'file_sub','layui-btn-normal','');?>
				  	</div>
				  	<input name="data[thumb]" class="thumb" type="hidden" lay-verify="thumb"/>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<?php echo admin_btn($add_url,'save','layui-btn-lg',"lay-filter='sub' location='$index_url'")?>
				</div>
			</div>
	</form>
	</div>
</div>
<?php echo template('admin/script');?>
<script type="text/javascript" src="<?php echo JS_PATH.'f_upload.js'?>" ></script>
<script type="text/javascript">
$(function(){
	$('.f_file').f_upload();
	$('#add_location').click(function(){
		var that = this;
		layer.open({
			type:2,
			content:'/adminct/location/iframe',
			area: ['60%', '60%'],
			btn:'选择插入', 
			btnAlign: 'c',
			yes:function(index){
				var body = layer.getChildFrame('body', index);
				$(body.find("input[name='xz']:checked")).each(function(k) {
					var name = $(this).data('name');
                    var id = $(this).val();
                    var h = '<input type="checkbox" name="data[lid]['+id+']" class="xl" title="'+name+'" value="'+id+'" checked>'
                    $(that).before(h);
                    layui.form.render('checkbox');
                });
				layer.closeAll('iframe');
			}
		});
	});
});
</script>
<?php echo template('admin/footer');?>