<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>番茄小程序全国招募合伙人</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,user-scalable=0" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="format-detection" content="telephone=no" />	
	<link rel="stylesheet" href="<?php echo CSS_PATH."mobile/vant.min.css";?>">
	<link rel="stylesheet" href="<?php echo CSS_PATH."mobile/style.css";?>">
	<style>
		img{width: 100%;}
	</style>
</head>
<body>
<div id="app" class="text-center" style="max-width: 900px;margin: auto;">
	<van-row class="g_ff">		
		<van-col span="24">	
			<div class="text-left p10">	
		  		<p style="line-height: 2; ">
					<span style=" font-size: 18px; color: rgb(255, 192, 0); background-color: rgb(0, 32, 96);">&nbsp; &nbsp;马上加微信咨询即可获得八折优惠！ &nbsp;</span>
					<br>
					<span style=" font-size: 18px; color: rgb(255, 192, 0); background-color: rgb(0, 32, 96);">&nbsp; &nbsp;微信号：13538436848 &nbsp;</span>
				</p>
				<p style="line-height: 2; ">
					<span style=" font-size: 18px; color: rgb(255, 192, 0); background-color: rgb(0, 32, 96);"><br/></span>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 16px;">番茄小程序是一个借助微信平台，不需要写代码，几分钟就可以生成一款属于你自己的小程序的一个神器；它还是一个拥有海量模板涉及到各行各业的一个小程序制作的神器。
					</span>
				</p>
				<p style="line-height: 2; ">
					<span style="color: rgb(51, 51, 51);  font-size: 18px; background-color: rgb(255, 255, 255);"><span style="font-size: 16px;"><br/></span></span>
				</p>
				<p style="line-height: 2; ">
					<strong><span style=" font-size: 18px; background-color: rgb(84, 141, 212); color: rgb(255, 255, 0);letter-spacing: 2px;">&nbsp;一 、为什么要做小程序？&nbsp;</span></strong>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 16px;">小程序可以快速帮助商家和企业完成互联网转型升级，通过线上各种营销模式为其获取更多收入。</span>
				</p>
				<br/>
				<p style="line-height: 2; ">
					<strong><span style="font-size: 18px; color: rgb(255, 255, 0); background-color: rgb(84, 141, 212);letter-spacing: 2px;">&nbsp;二、代理商招募对象&nbsp;</span></strong>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 16px;">1、学生和个人兼职</span>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 16px;">2、商家和企业OEM贴牌</span>
				</p>
				<br/>
				<p style="line-height: 2; ">
					<strong><span style="color: rgb(255, 255, 0); background-color: rgb(84, 141, 212); font-size: 18px;letter-spacing: 2px;">&nbsp;三、代理收益&nbsp;</span></strong><br/>
				</p>
				
				<p style="line-height: 2; ">
					<span style="font-size: 16px;  letter-spacing: 2px; text-align: center;">加入合伙人后，按最低标准计算，平均一个月开10个小程序，一个帐号平均3000，一年收入：（10x3000）x12 =360000 &nbsp;。</span>
				</p>
				<br/>
				<p style="line-height: 2; ">
					<strong>
					<span style="font-size: 18px; color: rgb(255, 255, 0); background-color: rgb(84, 141, 212);letter-spacing: 2px;">
						&nbsp;四、代理商优势，0加盟费，只需少量预存款&nbsp;
					</span>
					</strong>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 16px;"><strong><span style="  letter-spacing: 2px; text-align: center;">1、自主品牌：</span></strong><span style="  letter-spacing: 2px; text-align: center;">配套网站，独立运营，顶级域名赠送</span><strong><span style="  letter-spacing: 2px; text-align: center;"><br/></span></strong></span>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 16px;"><strong><span style="  letter-spacing: 2px; text-align: center;">2、无需技术：</span></strong><span style="  letter-spacing: 2px; text-align: center;">不需要技术研发团队，只需要营销团队即可接入</span></span>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 16px;"><strong><span style="  letter-spacing: 2px; text-align: center;">3、独立后台：</span></strong><span style="  letter-spacing: 2px; text-align: center;">代理商拥有自己的独立后台，管理名下所有客户</span></span>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 16px;"><strong><span style="  letter-spacing: 2px; text-align: center;">4、轻资产运作：</span></strong><span style="  letter-spacing: 2px; text-align: center;">让所有接入变成简单，移动互联网低成本运营</span></span>
				</p>
				<br/>
				<p style="line-height: 2; ">
					<strong >
						<span style="background-color: rgb(84, 141, 212);  letter-spacing: 2px;font-size: 18px;color: rgb(255, 255, 0);">&nbsp;五、番茄小程序覆盖行业&nbsp;</span>
					</strong>
				</p>
				<p style="line-height: 2; ">
					<span style="  letter-spacing: 2px; text-align: center; font-size: 16px;">餐饮、酒店、外卖、KTV、服饰、房产、商店、汽车、花店、美发、酒吧、家政、婚庆、医院、交通等多种行业。</span>
				</p>
				<br/>
				<p style="line-height: 2; ">
					<strong><span style=" background-color: rgb(84, 141, 212);  letter-spacing: 2px;font-size: 18px;color: rgb(255, 255, 0);">&nbsp;六、番茄小程序核心优势&nbsp;</span></strong>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 14px;"><strong><span style="  letter-spacing: 2px; text-align: center;">1、无需代码 自动生成：</span></strong><span style="  letter-spacing: 2px; text-align: center;">没有技术门槛，不懂代码也可立刻上手制作，5分钟快速生成小程序</span></span>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 14px;"><strong><span style="  letter-spacing: 2px; text-align: center;">2、可视化拖拽：</span></strong><span style="  letter-spacing: 2px; text-align: center;">拖拽式，操作简单，快速完成小程序制作</span></span>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 14px;"><strong><span style="  letter-spacing: 2px; text-align: center;">3、海量模板 持续更新：</span></strong><span style="  letter-spacing: 2px; text-align: center;">各种场景模板，组件 持续更新，组件轻松满足开发需求</span></span>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 14px;"><strong><span style="  letter-spacing: 2px; text-align: center;">4、多后台独立管理：</span></strong><span style="  letter-spacing: 2px; text-align: center;">每个小程序作品都拥有独立后台，用户可自由切换，实时管理</span></span>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 14px;"><strong><span style="  letter-spacing: 2px; text-align: center;">5、率先采用VUE技术：</span></strong><span style="  letter-spacing: 2px; text-align: center;">业界率先采用VUE前端框架技术打造极致轻量，稳定，快速</span></span>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 14px;"><strong><span style="  letter-spacing: 2px; text-align: center;">6、模块功能 随意组合：</span></strong><span style="  letter-spacing: 2px; text-align: center;">用户可以任意选择模块、功能、自由搭配，任意组合</span></span>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 14px;"><strong><span style="  letter-spacing: 2px; text-align: center;">7、全行业解决方案：</span></strong><span style="  letter-spacing: 2px; text-align: center;">可视化平台+行业方案+功能模块，完美解决所有用户需求</span></span>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 14px;"><strong><span style="  letter-spacing: 2px; text-align: center;">8、一键预览、发布审核：</span></strong><span style="  letter-spacing: 2px; text-align: center;">制作过程中，可实时预览，完成后一键发布审核</span></span>
				</p>
				<br/>
				<p style="line-height: 2; ">
					<strong><span style=" background-color: rgb(84, 141, 212);  letter-spacing: 2px;font-size: 18px;color: rgb(255, 255, 0);">&nbsp;七、售后保障&nbsp;</span></strong>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 14px;"><strong><span style="  letter-spacing: 2px; text-align: center;">1、服务器采用云服务器，高性能，分布式，稳定性保证所有小程序正常运营。</span></strong></span>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 14px;"><strong><span style="  letter-spacing: 2px; text-align: center;">2、系统成熟稳定，已为近10万家企业提供服务，累计用户达亿级访问。</span></strong></span>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 14px;"><strong><span style="  letter-spacing: 2px; text-align: center;">3、多人技术团队，功能更新迅速，提供6*8小时在线一对一售后和技术服务。</span></strong></span>
				</p>
				<p style="line-height: 2; ">
					<span style="font-size: 14px;"><strong><span style="  letter-spacing: 2px; text-align: center;">4、目前支持300余项功能，100<strong>余个应用场景皆可满足不同行业使用需求。</strong></span></strong></span>
				</p>
				<p>
					<span style="font-size: 14px;"><strong><span style="  letter-spacing: 2px; text-align: center;"><strong><br/></strong></span></strong></span>
				</p>
				</div>	
		</van-col>
		
		<van-col span="24">
			<h2 class="p10 f22">微信客服</h2>
		  	<p class="f18">13538436848</p>
		  	<p>微信搜索添加好友，或者留言给我们</p>
		</van-col>
		<van-col span="24" class="pl10 pr10">
			<div class="pt5 pb10">已经提交了286人</div>
			<van-cell-group>
			  <van-field v-model="username" placeholder="请输入姓名" ></van-field>
			  <van-field v-model="mobile" placeholder="请输入微信或者手机" ></van-field>
			</van-cell-group>
			<van-button round size="large" type="primary" @click="sub()" class="mt10">确认提交</van-button>
		</van-col>
	</van-row>
	<div class="mt60">&nbsp;</div>
</div>
<?php echo template('mobile/script');?>
<script>
var ajaxconfig = {headers: {'X-Requested-With': 'XMLHttpRequest','Content-Type': 'application/x-www-form-urlencoded'}};
new Vue({
	el: '#app',
	data: {
		username:'',
		mobile:'',
	},
	methods:{
		sub(){
  	  		if(this.username&&this.mobile){  	  	  		
  	  	  		var data = {"data[username]":this.username,"data[mobile]":this.mobile};
  	  	  	  	var l = this.$toast.loading({duration: 0,mask: true,message: '提交中...'});
  				axios.post('/m/hh/sub', Qs.stringify(data),ajaxconfig).then((response)=> {
  		  	  	  	var data = response.data;
  		  	  	  	l.clear();
  		  	      	if(data.state==1){  		  	  	    	
  			  	  	    this.$toast(data.message);
  		  	  	  	}else{
  			  	  	  	this.$toast(data.message);
  				  	}
  	  		  	    setTimeout(function(){
  	  	  		  	  location.href = "http://www.chaituans.com/daili.html";
  	  	  		  	    },3000);
  	  		  	    
  		  	    });
  	  	  	}else{
  	  	  	  	this.$toast.fail('请输入姓名和微信号');
	  	  		return ;
  	  	  	}  	  	  	
  	  	}
	}
});
</script>
</body>
</html>