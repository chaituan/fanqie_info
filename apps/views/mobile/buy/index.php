<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>微信小程序开发（限时优惠）</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0,user-scalable=0" />
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<meta name="format-detection" content="telephone=no" />	
	<link rel="stylesheet" href="<?php echo CSS_PATH."mobile/vant.min.css";?>">
	<link rel="stylesheet" href="<?php echo CSS_PATH."mobile/style.css";?>">
	<style>
		img{width: 100%;}
h1 {
    font-family:"微软雅黑";
    font-size:40px;
    margin:20px 0;
    border-bottom:solid 1px #ccc;
    padding-bottom:20px;
    letter-spacing:2px;
}
.time-item strong {
    background:#C71C60;
    color:#fff;
    line-height:49px;
    font-size:2em;
    font-family:Arial;
    padding:0 10px;
    margin-right:10px;
    border-radius:5px;
    box-shadow:1px 1px 3px rgba(0,0,0,0.2);
}

.item-title .unit {
    background:none;
    line-height:49px;
    font-size:24px;
    padding:0 10px;
    float:left;
}
	</style>
</head>
<body>
<div id="app" class="text-center" style="max-width: 900px;margin: auto;">
	<van-row class="g_ff">		
		<van-col span="24">			
		  	<img src="/res/images/buy/1.jpg">
		</van-col>
		<van-col span="24" class="mt15 mb15">			
		  	<h2 style="font-size: 3em">双11优惠倒计时</h2>
		  	<div class="time-item mt10">
			    <strong id="day_show">0天</strong>
			    <strong id="hour_show">0时</strong>
			    <strong id="minute_show">0分</strong>
			    <strong id="second_show">0秒</strong>
			</div>
		</van-col>
		<van-col span="24" class="mt15 pl10 pr10">
			<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin=262853419&site=qq&menu=yes">
				<van-button round size="large" type="primary" >下单QQ客服</van-button>
			</a>			
		</van-col>
		<van-col span="24" class="mt60 cr_888">			
		  	如果点击无效 ，请直接添加QQ号：262853419
		</van-col>
	</van-row>
	<div class="mt60">&nbsp;</div>
</div>
<?php echo template('mobile/script');?>
<script>
new Vue({
	el: '#app',
	
});
</script>
<script type="text/javascript">

var intDiff = parseInt(<?php echo $time?>);//倒计时总秒数量
function timer(intDiff){
    window.setInterval(function(){
    var day=0,
        hour=0,
        minute=0,
        second=0;//时间默认值        
    if(intDiff > 0){
        day = Math.floor(intDiff / (60 * 60 * 24));
        hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
        minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
        second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
    }
    if (minute <= 9) minute = '0' + minute;
    if (second <= 9) second = '0' + second;
    $('#day_show').html(day+"天");
    $('#hour_show').html('<s id="h"></s>'+hour+'时');
    $('#minute_show').html('<s></s>'+minute+'分');
    $('#second_show').html('<s></s>'+second+'秒');
    intDiff--;
    }, 1000);
} 
$(function(){
    timer(intDiff);
}); 
</script>
</body>
</html>