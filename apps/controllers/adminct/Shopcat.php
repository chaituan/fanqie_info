<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 分类
 * @author chaituan@126.com
 */
class Shopcat extends AdminCommon {
	public function __construct() {
		parent::__construct ();
		$this->load->model ('admin/ShopCat_model','do');
	}
	
	function index() {
		$this->load->view ('admin/shopcat/index');
	}
	
	function lists(){
		$name = Gets('name');//搜索
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"news.title like '%$name%'":'';
		$data = $this->do->getItems ($where,'','sort',$page,$limit,$total);
		$find = Gets('find');//mark 为了 第一次查询请求判断，
		if(($name&&$find)||!$total){//当name和find有值的时候，代表是第一次查询，分页点击只有find为空值
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	public function add() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			if ($this->do->add($data)) {
				$this->do->cache();
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$this->load->view ('admin/shopcat/add');
		}
	}
	
	public function edit() {
		if (is_ajax_request()) {
			$data = Posts('data');
			if ($this->do->updates($data,"id=".Posts('id','checkid'))) {
				$this->do->cache();
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$data['item'] = $this->do->getItem(array('id' => Gets('id','checkid')));
			$this->load->view('admin/shopcat/edit',$data);
		}
	}
	
	function edits(){
		if(is_ajax_request()){
			$data = Posts('data');
			is_AjaxResult($this->do->updates($data,"id=".Posts('id','checkid')));
		}
	}
	
	public function del() {
		if($this->do->deletes("id=".Gets('id','checkid'))){
			$this->do->cache();
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
}
