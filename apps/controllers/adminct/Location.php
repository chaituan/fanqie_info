<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 区域
 * @author chaituan@126.com
 */
class Location extends AdminCommon {
	
	public function __construct() {
		parent::__construct ();
		$this->load->model ('admin/Location_model','do');
	}
	
	public function index() {
		$this->load->view('admin/location/index');
	}
	
	public function iframe() {
		$data['name'] = $name = Gets('name');//搜索
		$where = $name?"lname like '%$name%'":'';
		$data['items'] = $this->do->getItems($where);
		$this->load->view('admin/location/iframe', $data);
	}
	
	function lists(){
		$name = Gets('name');
		$page = Gets('page','checkid');$limit = Gets('limit','checkid');
		$total = Gets('total','num');
		$where = $name?"lname like '%$name%'":'';
		$data = $this->do->getItems($where,'','',$page,$limit,$total);
		$find = Gets('find');
		if(($name&&$find)||!$total){
			$total = $this->do->count;
		}
		f_ajax_lists($total, $data);
	}
	
	public function add() {
		if (is_ajax_request ()) {
			$data = Posts('data');
			if ($this->do->add($data)) {
				$this->do->cache();
				AjaxResult_ok();
			} else {
				AjaxResult_error();
			}
		} else {
			$this->load->view ('admin/location/add');
		}
	}

	
	function edits(){
		if(is_ajax_request()){
			$data = Posts('data');
			is_AjaxResult($this->do->updates($data,"id=".Posts('id','checkid')));
		}
	}
	
	public function del() {
		$id = Gets('id','checkid');
		if($this->do->deletes("id=$id")){
			$this->do->cache();
			AjaxResult_ok();
		}else{
			AjaxResult_error();
		}
	}
	
	function getl(){
		if (is_ajax_request()) {
			$ip = Posts('ip');
			$ip = explode(',', $ip);
			$la = $ip[0];$lg = $ip[1];
			$ipget = file_get_contents("http://apis.map.qq.com/ws/geocoder/v1/?location=$la,$lg&key=XNXBZ-H443D-SXS4W-HXFSF-KQTLH-FKFAW");
			if(!$ipget)AjaxResult_error('接口上限，无法定位');
			$result = json_decode($ipget,true);
			if($result['status']==0){
// 				$city = $result['result']['address_reference']['town']['title'];
				$city = $result['result']['address_component']['city'];
				AjaxResult(1, 'ok',$city);
			}else{
				AjaxResult_error('查询失败');
			}
		}
	}
}
