<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * @author chaituan@126.com
 */
class News extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		parseURL($this->uri->segment(4));
	}
	
	function detail(){
		$data['id'] = Gets('id');
		$this->load->model(array('admin/News_model'));
		$data['item'] = $this->News_model->getItem($data,'title,addtime,content');
		$this->load->view('mobile/news/detail',$data);
	}
}
