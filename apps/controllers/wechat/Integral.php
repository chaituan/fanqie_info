<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
/**
 * 积分
 * @author chaituan@126.com
 */
class Integral extends WechatCommon {
	

	public function index() {
		if(is_ajax_request()){
			$uid = $this->User['id'];
			$this->load->model(array('admin/Integral_model'=>'do','admin/User_model'=>'user'));
			$jf = $this->do->getItems(array('uid'=>$uid),'','id desc');
			if($jf){
				foreach ($jf as $v){
					$v['addtime'] = format_time($v['addtime'],'Y-m-d H:i');
					$items[] = $v;
				}
				$user = $this->user->getItem(array('id'=>$uid),'integral');
				$data['integral'] = $user['integral'];
				$data['items'] = $items;
				AjaxResult(1, 'ok',$data);
			}else{
				AjaxResult_error('没有数据');
			}
		}
			
		
	}

}
