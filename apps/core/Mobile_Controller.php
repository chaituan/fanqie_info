<?php
if (! defined ( 'BASEPATH' ))exit ( 'No direct script access allowed' );
/**
 * 移动端公共类
 * @author chaituan@126.com
 */
class WechatCommon extends CI_Controller {
	protected $User;
	function __construct() {
		parent::__construct ();
		$this->User = $this->session->wechat_user_session;
		$this->check();
	}
	
	function check() {
		if ($this->User) {
			
		} else {			
			AjaxResult(5, '登录过期，请重新登录');
		}
	}
}
