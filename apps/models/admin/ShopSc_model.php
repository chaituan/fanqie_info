<?php if (! defined ( 'BASEPATH' ))	exit ( 'No direct script access allowed' );
/**
 * 店铺收藏
 * @author chaituan@126.com
 */
class ShopSc_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'shop_sc';
	}
}