<?php if (! defined ( 'BASEPATH' ))	exit ( 'No direct script access allowed' );
/**
 * 卡片人气
 * @author chaituan@126.com
 */
class CardRq_model extends MY_Model {
	function __construct() {
		parent::__construct ();
		$this->table_name = 'card_rq';
	}
}