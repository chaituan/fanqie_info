<?php 
/**
 * chaituan@126.com
 */
class Images{
	// 背景图路径
    public $bpath;
    // 背景宽
    public $bw;
    // 背景高
    public $bh;
    // 背景图类型
    public $btype;
    // 背景对象
    protected $bobj;
    // 水印路径
    public $wpath;
    // 新图宽
    public $nw;
    // 新图高
    public $nh;
    // 水印位置
    public $positon = 4;
    // 图片透明度 1 - 100
    public $alpha = 100;

    // 构造函数 将背景图路径传进去就好 
    public function __construct($bpath){   
        // 获取背景图信息
        $bres = $this->getImgInfo($bpath[0]);
        $this->bpath = $bpath[0];
        $this->bw = $bres['w'];
        $this->bh = $bres['h'];
        $this->btype = $bres['type'];
    }

    /**
     * 根据图片类型创建对象
     * @param  string 图片类型
     * @return obj
     */
    public function createObj($path){
        $info = $this->getImgInfo($path);
        $type = $info['type'];
        $func = 'imagecreatefrom'.$type;
        return $func($path);
    }

    /**
     * 获取图片尺寸
     * @param  [type] $path [description]
     * @return [type]       [w:宽, h：高,type :类型]
     */
    public function getImgInfo($path){
        $info = @getimagesize($path);
        $res['w'] = $info[0];
        $res['h'] = $info[1];
        $res['type'] = image_type_to_extension($info[2],false);
        return $res;
    }

   
    /**
     * 创建单图片水印
     * @param $wpath 水印图路径
     * @return [type] [description]
     */
    public function createWate($wpath,$pores,$savefile)
    {
        // 创建图片对象
        $bobj = $this->createObj($this->bpath);
        $wobj = $this->createObj($wpath);
//         $pores = $this->getPosition($wpath);
//         if($pores['error']){
//             return $pores['error'];
//         }
        $x = $pores['x'];
        $y = $pores['y'];
        $w = $pores['w'];
        $h = $pores['h'];
        // 原图obj ， 水印图obj ,水start_x , 水start_y, 水x , 水y ，拷贝的宽度, 拷贝的高度 , 透明度
        $res = imagecopymerge($bobj, $wobj, $x, $y, 0, 0, $w, $h, $this->alpha);
        
        imagejpeg($bobj, $savefile); 
        
        imagedestroy($bobj); 
        imagedestroy($wobj);  
        
        return $savefile;
    }

    /**
     * 创建多个图片水印
     * @param  [array] $waterpaths [水印图路径]
     * @param  [array] $positon [水印位置]
     * @param  [array] $alpha [水印透明度]
     * @param  [int]   $re    [缩放值 0 - 1]
     * @return [type]             [description]
     * 使用方式
     * $res =$obj->createWates([$waterpath,'./img/share.png'],[1,3],[40,50]);
     */
    public function createWates($waterpaths,$savefile,$alpha = 100)
    {   
        // 创建背景对象
        $bobj = $this->createObj($this->bpath);
        $deobjs[] = $bobj;
        foreach ($waterpaths as $key => $value) {
            $deobjs[] = $nobj = $this->createObj($value['bobj']);
            $x = $value['pores']['x'];
            $y = $value['pores']['y'];
            $w = $value['pores']['w'];
            $h = $value['pores']['h'];
            // 原图obj ， 水印图obj ,水start_x , 水start_y, 水x , 水y ，拷贝的宽度, 拷贝的高度 , 透明度
            imagecopymerge($bobj, $nobj, $x, $y, 0, 0, $w, $h, $alpha);       
        }
        
        imagejpeg($bobj, $savefile);
      
        foreach ($deobjs as $v) {
            imagedestroy($v); 
        }
        return '';
    }

    /**
     * 创建缩放的图片
     * @param  图片路径
     * @return obj
     */
    public function createReObj($filepath)
    {
        // 获得原来的图片大小  
        list($width, $height) = getimagesize($filepath);
        $this->nw = $width * $this->percent;  
        $this->nh = $height * $this->percent;
        // 原比例
        $ratio_orig = $width / $height;
        // 新比例
        $neww = $this->nw / $this->nh;
        // 源图像资源
        $reobj = $this->createObj($filepath);
        // 分配颜色
        $image = imagecreatetruecolor($this->nw, $this->nh); 
        // 填充背景
        $white = ImageColorAllocate($image,255,255,255);
        // 开始缩放
        imagecopyresampled($image, $reobj, 0, 0, 0, 0, $this->nw, $this->nh, $width, $height);
        imagepng($image);
    }
    
    /**
     * 模糊定位，获取水印图相对于背景图的 x,y
     * @return [type] [description]
     */
    public function getPosition($wpath,$p ='') {
    	$binfo = getimagesize($this->bpath);
    	// echo "<pre>";
    	// 获取水印图信息
    	$waterinfo = getimagesize($wpath);
    	/*原图必须大于水印*/
    	if($binfo[0] < $waterinfo[0]){
    		return ['error'=>'水印图过大'];
    	}
    	if($binfo[1] < $waterinfo[1]){
    		return ['error'=>'水印图过高'];
    	}
    	$positon = empty($p) ? $this->positon : $p;
    	switch ($positon) {
    		//1顶部居左
    		case 1:
    			$x=$y=0;
    			break;
    			//2顶部居右
    		case 2:
    			// 原图宽 - 水印图宽
    			$x = $binfo[0] - $waterinfo[0];
    			$y = 0;
    			break;
    			//3居中
    		case 3:
    			$x = ($binfo[0] - $waterinfo[0])/2;
    			$y = ($binfo[1] - $waterinfo[1])/2;
    			break;
    			//4底部居左
    		case 4:
    			$x = 0;
    			$y = $binfo[1] - $waterinfo[1];
    			break;
    			//5底部居右
    		case 5:
    			$x = $binfo[0] - $waterinfo[0];
    			$y = $binfo[1] - $waterinfo[1];
    			break;
    		default:
    			$x=$y=0;
    	}
    	return ['x'=>$x,
    	'y'=>$y,
    	'w'=>$waterinfo[0],
    	'h'=>$waterinfo[1],
    	'error'=>false
    	];
    }
}