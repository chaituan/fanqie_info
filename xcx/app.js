//app.js
App({
  onLaunch: function (o) {
    this.globalData.fid = o.query.scene;
  },
  reg: function () {
    var is_login = wx.getStorageSync('PHPSESSID');
    if (!is_login) {
      this.login();
      return false;
    }
    return true;
  },
  login: function () {
    wx.navigateTo({
      url: '/pages/login/index',
    })
  },
  again: function (state) {
    if (state == 5) {
      wx.showLoading({ title: '重新登录中...', mask: true })
      this.login();
    }
  },
  header: function (sid) {
    if (sid) {
      return { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest', 'Cookie': 'hbsq_session=' + sid };
    } else {
      wx.showToast({ title: '登录失败', icon: 'none' })
    }
  },
  globalData: {
    host: ""//这里写您的域名
  },
  imageLoad: function (e, image,mark) {
    var $width = e.detail.width, $height = e.detail.height, ratio = $width / $height;    //图片的真实宽高比例
    var viewWidth = mark ? 750 - mark : 750, viewHeight = (mark ? 750 - mark : 750) / ratio;    //计算的高度值
    //将图片的datadata-index作为image对象的key,然后存储图片的宽高值
    image[e.target.dataset.index] = {
      style: "width:" + viewWidth + "rpx; height:" + viewHeight+"rpx;"
    }
    return image;
  }
})