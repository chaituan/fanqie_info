const app = getApp();//店铺管理中心
Page({
  data: {

  },
  onLoad: function (options) {

  },
  onReady: function () {

  },
  onShow: function () {
    if (app.reg()) {
      this.setData({ user: wx.getStorageSync('userinfo'), uid: parseInt(wx.getStorageSync('FXID')) });
      this.get_shop();
    } 
  },
  get_shop:function(){
    wx.showLoading({ title: '玩命加载中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/shop_api/shop',
      data: { uid: this.data.uid },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.setData({ shop: d.data.data });
          wx.setStorageSync('myshop', d.data.data);
        }
        wx.hideLoading();
      }
    });
  },
  creat:function(){//创建并编辑店铺
    var id = this.data.shop ? this.data.shop.id : 0;
    if (id) {
      var url = "/pages/shop/issue/index?id=" + id;
    } else {
      var url = "/pages/shop/issue/index";
    }
    wx.navigateTo({
      url: url,
    })
  },
  url:function(e){
    var url = e.currentTarget.dataset.url;
    wx.navigateTo({
      url: url,
    })
  },
  urls: function (e) {
    var url = e.currentTarget.dataset.url;
    wx.switchTab({
      url: url,
    })
  },
  onHide: function () {

  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {

  },
  onReachBottom: function () {

  },

 
})