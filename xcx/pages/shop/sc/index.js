const app = getApp();
Page({
  data: {
    fy: { page: 1, count: 0, end: 1 }//分页
  },
  onLoad: function (options) {    
    var uid = parseInt(wx.getStorageSync('FXID'));
    if(uid){
      this.setData({ uid: uid });
      this.get_lists();  
    }      
  },
  onReady: function () {
    
  },
  onShow: function () {    
    
  },
  get_lists:function(){
    wx.showLoading({ title: '玩命加载中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/shop_api/shop_sc_lists',
      data: { uid: this.data.uid, page: this.data.fy.page, total: this.data.fy.count },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.data.fy = d.data.mark;
          this.setData({ shop: this.data.shop ? this.data.shop.concat(d.data.data) : d.data.data });
        }else{
          this.data.fy.end = 0;
          this.setData({ msg: d.data.message });
        }
        wx.hideLoading();
      }
    });
  },
  onDel:function(e){
    var id = e.currentTarget.id;
    var index = e.currentTarget.dataset.index;
    var gz = "shop[" + index + "].gz";
    wx.showLoading({ title: '取消中...', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/shop_api/shop_sc',
      data: { id: id, query: 'del' },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          var gz = "shop["+index+"].gz";
          this.setData({ [gz]: 1});
        } else {
          wx.showToast({
            title: d.data.message,
          })
        }
        wx.hideLoading();
      }
    })
  },
  onHide: function () {

  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {

  },
  onReachBottom: function () {
    this.get_lists();
  },
  resetFy: function () {//重置分页
    this.data.fy = { page: 1, count: 0, end: 1 };
    this.data.shop = '';
  }

})