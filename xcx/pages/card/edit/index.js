const app = getApp();
Page({

  data: {
    iid: 1, address: '', readonlys:true
  },
  onLoad: function (options) {
    this.setData({ query: options.id ? 'edit' : 'add', id: options.id,user: wx.getStorageSync('userinfo'), uid: parseInt(wx.getStorageSync('FXID'))});
    if (options.id){
      var getdata = wx.getStorageSync('mycard');
      for(var k in getdata){
        this.setData({ [k]: getdata[k]});
      }      
    }
  },
  onReady: function () {
   
  },
  onShow: function () {
    if (app.reg()) {           
      var pages = getCurrentPages();
      var currpage = pages[pages.length - 1];
      if (currpage.data.city || currpage.data.industry) {
        if (currpage.data.city) {
          this.setData({ cid: currpage.data.city.id, cname: currpage.data.city.text });
        } 
        if (currpage.data.industry) {
          this.setData({ iid: currpage.data.industry.id, iname: currpage.data.industry.text });
        }
      }
    }  
  },
  upload: function () {
    wx.showLoading({ title: '打开相册中', mask: true });
    var that = this;
    wx.chooseImage({
      sizeType: ['compressed'],
      sourceType: ['album'],
      count:1,
      success: function (res) {
        that.setData({ thumb: res.tempFilePaths[0],thumb_state:1});
      },
      complete: function () {
        wx.hideLoading();
      }
    })
  },
  onChange:function(e){
    this.setData({[e.currentTarget.dataset.name]:e.detail});
  },
  onSub:function(){
    var yz = this.yanzen();
    if(yz){
      if(this.data.query=='edit'&&!this.data.thumb_state){
        this.formSub();
      }else{
        this.send();   
      }
    }
  },
  yanzen:function(){
    var say = "";
    if(!this.data.thumb) {
      say = "请上传头像";
    }else if(!this.data.username){
      say = "姓名不能为空";
    } else if (!this.data.mobile){
      say = "电话不能为空";
    } else if (!this.data.company) {
      say = "公司不能为空";
    } else if (!this.data.bm) {
      say = "部门不能为空";
    } else if (!this.data.position) {
      say = "职位不能为空";
    } else if (!this.data.zz) {
      say = "职责不能为空";
    } else if (!this.data.address) {
      say = "位置不能为空";
    } else if (!this.data.iid) {
      say = "行业不能为空";
    } else if (!this.data.cid) {
      say = "城市不能为空";
    }
    if (say){
      wx.showToast({title: say,icon:'none',mask:true});
      return false;
    }
    return true;
  },
  send: function () {
    wx.showToast({ title: '创建中...', icon: 'icon',mask:true });
    const uploadTask = wx.uploadFile({
      url: app.globalData.host + 'wechat/api/issue_upimg',
      filePath: this.data.thumb,
      name: 'file',
      header: {"Content-Type": "multipart/form-data", 'X-Requested-With': 'XMLHttpRequest'},
      success: (res) => {
        var res_data = JSON.parse(res.data);        
        if (res_data.state == 1) {
          this.setData({ thumb: app.globalData.host + res_data.data});
          this.formSub();
        }else{
          wx.showToast({ title: '上传失败', icon: 'icon' });
        }       
      },
      fail: (res) => {
        wx.showToast({title: '上传失败',icon:'icon'});
      }
    });
    uploadTask.onProgressUpdate((res) => {
      wx.showLoading({ title: '上传 ' + res.progress + ' %', mask: true });
    })
  },
  formSub:function(){
    var thumb = this.data.thumb.replace(app.globalData.host.substring(0, app.globalData.host.length - 1),'');
    var card = { thumb: thumb, username: this.data.username, mobile: this.data.mobile, company: this.data.company, bm: this.data.bm,
      position: this.data.position, zz: this.data.zz, address: this.data.address, iid: this.data.iid, iname: this.data.iname, cid: this.data.cid, cname: this.data.cname, 
     uid: this.data.uid, la: this.data.la, lg: this.data.lg, query: this.data.query 
     }
    if(this.data.query=='edit'){
      card['id'] = this.data.id;
    }
    wx.showLoading({ title: '玩命发布中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/card_api/card_update',
      data: card,
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          if (this.data.query == 'add') {
            card['id'] = d.data.message;
          }
          card['thumb'] = app.globalData.host + card['thumb'];
          wx.setStorageSync('mycard', card);
          wx.showModal({
            title: '操作成功',
            showCancel: false,
            content: '分享给朋友或者微信群，让更多的人知道您！',
            success:res=>{              
              wx.navigateBack({});
            }
          })          
        }else{
          wx.showToast({ title: d.data.message, icon: 'none' });
        }
        wx.hideLoading();
      }
    });
  },
  onMap: function () {
    wx.chooseLocation({
      success: (res)=> {
        if (res.errMsg == 'chooseLocation:ok' && res.address) {
          this.setData({
            address: res.address, la: res.latitude, lg: res.longitude, readonlys:false
          });
        }
      },
      fail: res => {//用户拒绝，则提示重新获取
        this.setData({ dialogshow: true });     
      }
    })
  },
  onYx: function () {
    wx.openSetting({
      success: res2 => {
        if (res2.authSetting["scope.userLocation"]) {//如果用户重新同意了授权登录
          wx.showToast({ title: '授权成功', duration: 2000 });
          setTimeout(() => {  //要等2秒 才能收到数据
            this.onMap();
          }, 2000);
        }
      },
      fail: f => {
        console.log(f)
      }
    })
  },
  onYxno: function () {
    wx.showToast({
      title: '无法正常使用',icon:'none'
    })
  },
  onHide: function () {

  },
  onUnload: function () {

  },
  onPullDownRefresh: function () {

  },
  onReachBottom: function () {

  }
})