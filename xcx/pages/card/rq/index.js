const app = getApp();
Page({

  data: {
    state: 1,
    fy: { page: 1, count: 0, end: 1 }//分页
  },

  onLoad: function (options) {
    this.setData({ user: wx.getStorageSync('userinfo'), uid: parseInt(wx.getStorageSync('FXID')) });
    this.get_lists();
  },

  onReady: function () {

  },

  onShow: function () {
    if (app.reg()) {
      
    }
  },
  get_lists: function () {
    if (!this.data.fy.end) return false;
    wx.showLoading({ title: '玩命加载中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/card_api/card_rq_lists',
      data: { uid: this.data.uid, state: this.data.state, page: this.data.fy.page, total: this.data.fy.count },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.data.fy = d.data.mark;
          this.setData({ lists: this.data.lists ? this.data.lists.concat(d.data.data) : d.data.data });
        } else {
          this.data.fy.end = 0;
          this.setData({ lists: '', msg: d.data.message });
        }
        wx.hideLoading();
      }
    });
  },
  onTab: function (e) {
    this.setData({ state: e.detail.index });
    this.resetFy();
    this.get_lists();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.get_lists();
  },
  resetFy: function () {//重置分页
    this.data.fy = { page: 1, count: 0, end: 1 };
    this.data.lists = '';
  }
})