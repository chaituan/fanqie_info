const app = getApp();
var cache = require('../../../utils/cache.js');
Page({
  data: {
    tabbar: 1,
    actionshow:false,
    actions: [{ name: '人气榜', num: 1 }, { name: '点赞榜', num: 2 }, { name: '关注榜', num: 3 }, { name: '附近的人', num: 4 }, { name: '最新名片', num: 5 }],
    phname:'最新名片',
    cityname:'所有区域',
    industryname:'所有行业',
    where:{},
    fy: { page: 1, count: 0, end: 1 }//分页
  },
  onLoad: function (options) {
    var uid = wx.getStorageSync('FXID') ? parseInt(wx.getStorageSync('FXID')) : 0;
    this.setData({ uid: uid });
    this.data.where['phnum'] = 5;
    var mycard = wx.getStorageSync('mycard');
    if (mycard) {
      this.setData({ mycard: mycard });
    } else {
      this.get_card();
    }
    this.get_lists();    
  },
  onReady: function () {
    
  },
  onShow: function () {
    
      this.setData({ mycard: wx.getStorageSync('mycard') }); 
      var pages = getCurrentPages();
      var currpage = pages[pages.length - 1];
      if (currpage.data.city || currpage.data.industry) {
        this.resetFy();
        if (currpage.data.city){
          var name = currpage.data.city.text;
          this.setData({ city: currpage.data.city, cityname: name.length > 5 ? name.substr(0, 5) + '...' : name });
          this.data.where['city'] = currpage.data.city.id;
        }        
        if (currpage.data.industry) {
          var name = currpage.data.industry.text;
          this.setData({ industry: currpage.data.industry, industryname: name.length > 5 ? name.substr(0, 5) + '...' : name });
          this.data.where['industry'] = currpage.data.industry.id;
        }
        this.get_lists();
      }      
         
  },
  get_card: function () {
    wx.showLoading({ title: '玩命加载中', mask: true });
    wx.request({
      url: app.globalData.host + 'wechat/card_api/card',
      data: { uid: this.data.uid },
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.setData({ mycard: d.data.data });
          wx.setStorageSync('mycard', d.data.data);
        }
        wx.hideLoading();
      }
    });
  },
  get_lists: function () {
    if (!this.data.fy.end) return false;
    wx.showLoading({ title: '玩命加载中', mask: true });
    this.data.where['uid'] = this.data.uid;  
    this.data.where['page'] = this.data.fy.page, this.data.where['total'] = this.data.fy.count  ;
    wx.request({
      url: app.globalData.host + 'wechat/card_api/card_top_lists',
      data: this.data.where,
      method: 'POST',
      header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
      success: d => {
        if (d.data.state == 1) {
          this.data.fy = d.data.mark;
          this.setData({ lists: this.data.lists ? this.data.lists.concat(d.data.data) : d.data.data });
        } else {
          this.data.fy.end = 0;
          this.setData({ lists:'',msg: d.data.message });
        }
        wx.hideLoading();
      }
    });
  },
  onDetail:function(e){
    var id = e.currentTarget.id;
    wx.navigateTo({
      url: '/pages/card/top/detail?id=' + id + "&uid=" + this.data.mycard.uid,
    });
  },
  onMycard: function () {
    wx.navigateTo({
      url: '/pages/card/index'
    });
  },
  onPh:function(){
    this.setData({ actionshow:true });
  },
  onClose() {
    this.setData({ actionshow: false });
  },
  onSelect(event) {
    var d = event.detail;
    this.setData({ actionshow: false,phname:d.name,num:d.num });
    this.resetFy();
    //人气榜
    this.data.where['phnum'] = d.num;
    if (d.num==4){//附近的人
      var location = cache.get('location');
      if (location) {//本地缓存的地址信息    
        this.setData({ location });
        this.data.where['la'] = location.la;
        this.data.where['lg'] = location.lg;
        this.get_lists();
      } else {//获取最新的地址，并执行获取数据
        this.get_location();
      } 
    }else{
      this.get_lists();
    }
    
  },
  onCity:function(){
    wx.navigateTo({
      url: '/pages/city/index?show=1',
    })
  },
  onIndustry:function(){
    wx.navigateTo({
      url: '/pages/industry/index?show=1',
    })
  },
  get_location() {
    wx.showLoading({
      title: '位置计算中',
    })
    wx.getLocation({
      success: res => {//正常获取    
        var la = res.latitude, lg = res.longitude;
        cache.put('location', { la: la, lg: lg }, 43200);//存入缓存 过期时间1天
        this.data.where['la'] = location.la;
        this.data.where['lg'] = location.lg;
        this.get_lists();
      },
      fail: res => {//用户拒绝，则提示重新获取
        this.setData({ dialogshow: true });
      },
      complete: function () {
        wx.hideLoading();
      }
    });

  },
  onYx: function () {
    wx.openSetting({
      success: res2 => {
        if (res2.authSetting["scope.userLocation"]) {//如果用户重新同意了授权登录
          wx.showToast({ title: '授权成功', duration: 2000 });
          setTimeout(() => {  //要等2秒 才能收到数据
            this.get_location();
          }, 2000);
        }
      },
      fail: f => {
        console.log(f)
      }
    })
  },
  onYxno: function () {
    this.data.fy.end = 0;
    this.setData({ lists: '', msg: '无法使用此功能!~~~~(>_<)~~~~' });
  },
  onHide: function () {

  },
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.get_lists();
  },
  resetFy: function () {//重置分页
    this.data.fy = { page: 1, count: 0, end: 1 };
    this.data.lists = '';    
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})