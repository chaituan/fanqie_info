const app = getApp()
Page({
  data: {

  },
  onLoad: function () {

  },
  userInfoHandler: function (e) {
    wx.showLoading({ title: '登录中...' });
    wx.login({
      success: r1 => {
        wx.getUserInfo({
          withCredentials: true,
          success: r2 => {
            if (app.userInfoReadyCallback) {
              app.userInfoReadyCallback(r2);
            }
            wx.request({
              url: app.globalData.host + 'wechat/login/index',
              data: { code: r1.code, info: JSON.stringify(r2.userInfo), fid: app.globalData.uid },
              method: 'POST',
              header: { 'Content-Type': 'application/x-www-form-urlencoded', 'X-Requested-With': 'XMLHttpRequest' },
              success: function (d) {
                wx.hideLoading();
                if (d.data.state == 1) {
                  wx.setStorageSync('FXID', d.data.data.uid);
                  wx.setStorageSync('PHPSESSID', d.data.data.sid);
                  wx.setStorageSync('userinfo', r2.userInfo);
                  wx.showToast({ title: '登录成功', duration:2000 });
                  setTimeout(function () {
                    var pages = getCurrentPages();
                    var page = pages[pages.length - 2];
                    var params = page.options ? page.options : '';
                    var tab = page.data.tabbar ? true : false;
                    if (tab) {
                      wx.switchTab({
                        url: '/' + page.route
                      })
                    } else {
                      wx.navigateBack({
                        success: function () {//登录成功后返回，当有参数的时候直接设置，返回的页面会立即执行刷新
                          if (params) {
                            page.setData(params);
                          }
                          page.onLoad();
                        }
                      })
                    }
                  }, 2000);
                } else {
                  wx.showModal({
                    title: '登录错误（1）',
                    content: d.data.message,
                  });
                }
              }
            })
          }, fail: function (e) {
            wx.hideLoading();
            wx.showModal({
              title: '登录错误',
              content: e.errMsg,
            });
          }
        })
      }
    })
  }
})